# Sonar rock vs mine detection using LR
# By- Aarush Kumar
Detection of sonar rock vs mines using LogisticRegression.
Description of the Dataset:
The dataset we will use in this tutorial is the Sonar dataset.
This is a dataset that describes sonar chirp returns bouncing off different services. The 60 input variables are the strength of the returns at different angles. It is a binary classification problem that requires a model to differentiate rocks from metal cylinders.
And finally used LogisticRegression in order to train model and perform further operations.
Thankyou!
